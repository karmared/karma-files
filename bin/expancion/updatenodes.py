#!/usr/bin/env python3
#!/usr/bin/env python36
# encoding: utf-8
# MILE
from sshreader import SSH, ServerJob, Hook, sshread, utils, shell_command
from sshreader.ssh import envvars
import os, sys, time, shlex, subprocess, argparse, random
from subprocess import Popen, PIPE, STDOUT
from ruamel.yaml import YAML

def createParser (): # {{{ parse Argument
    parser = argparse.ArgumentParser(
        description='The utility on distribution and support of the blockchain nodes',
        epilog='Think that you do!'
    )
    parser.add_argument (
        '-c','--command',choices=[
        'start','stop','restart','erase','install','update','status','copylog'
        ],default='status',
        help='"start" - Run daemon sumus \n "stop" - Terminate daemon sumus'
    )
    parser.add_argument ('--external', help="testnet from globalnet",
                         action="store_true")
    parser.add_argument ('--createconfig', help="create template config file",
                         action="store_true")
    parser.add_argument ('--config', help="use config no default file",
                         default="updatenodes.conf")
    parser.add_argument ('--host', help="use only <hostname>")
    return parser # }}}
def useconfig(objconfig=None, create=True): # {{{ create template config file and check usage file
    yaml = YAML()
    global namespace
    hosts=dict()
    urlsumus = "ftp://192.168.10.49/download/RPMBUILD/RPMS/"
    abspatch = os.getcwd()
    for apnd in range(0,10): # {{{generate list work nodes
            url = 'node{:02d}.testnet.mile.global'.format(apnd)
            hosts.update({url: dict(zip(['type', 'wallet'], [
                'work', # work / passive / kafka /json node
                'node{:05d}.wallet'.format(apnd) # wallet file
                ])),
            }) # }}}
    for apnd in range(0,3): # {{{ generate list sn node
            url = 'lotus{:02d}.testnet.mile.global'.format(apnd)
            hosts.update({url: dict(zip(['type', 'wallet'], [
                'passive', # work node
                None, #wallet file
                ])),
            }) # }}}
    obj = {
           'comments':'The utility on distribution and support of the blockchain nodes', #комментарии
           'namesumus':namesumus, #тип блокчейна
           'versumus':versumus, #версия ПО
           'workdir':abspatch, #рабочая директория
           'modir':'/Users/a/WorkSpace/FuncTest/modules',
           'shell':abspatch+'/'+'sumus/shell.mile-debug',
           'pemfile':'testnet.mile.global.pem', #ssl cert/key container from https
           'external': namespace.external,   #локальная работа
           'addoptionstartnodework':'',
           'addoptionstartnodesn':'--passive',
           'urlsumus':urlsumus,
           'NODES':hosts,
        }
    if create:
        with open(namespace.config, 'wb') as f:
            yaml.dump(obj, f )
    else:
        errorflag = True
        if not objconfig: return False
        for key in obj:
            if objconfig.get(key, 'NotKeyAndValue') == 'NotKeyAndValue':
                print("Config error, not fount field:", key)
                errorflag = False
        return errorflag
        # }}}
def sshtransfer(job): # {{{ transfer files from  node
    global abspatch
    global namespace
    global hosts
    global pemfile
    global namesumusBC
    #global obj
    #hosts     = obj['NODES']
    with SSH(job.name,username='root',password=None) as s:
        if "install" in namespace.command:
            s.sftp_put(abspatch+'/genesis_block.txt',
                  '/root/genesis_block.txt')
            s.sftp_put(abspatch+'/sumus/'+'shell.'+namesumus+'-debug',
                  '/root/shell-debug')
            s.sftp_put(abspatch+'/sumus/'+'shell.'+namesumus,
                  '/root/shell')
        if  namespace.external:
            s.sftp_put(abspatch+'/'+namesumusBC,
                      '/root/'+namesumusBC)
        if hosts[job.name]['type'] == 'work' : #work nodes
            s.sftp_put(abspatch+'/wallets/'+str(hosts[job.name]['wallet']),
                  '/root/'+str(hosts[job.name]['wallet']))#copy wallet key
        else: #sn nodes
            if hosts[job.name]['type'] == 'kafka':
                global kafkaproxy
                s.sftp_put(abspatch+'/'+kafkaproxy,
                      '/root/'+kafkaproxy)
            if pemfile:
                s.sftp_put(abspatch+'/sumus/wallet-jsonrpcd',
                    '/root/wallet-jsonrpcd')
                s.sftp_put(abspatch+'/'+pemfile,
                    '/root/'+pemfile)
# }}}
def sshobtaining(job): # {{{ transfer files from local
    global abspatch
    with SSH(job.name,username='root',password=None) as s:
        s.sftp_get('/var/log/sumus/export.log.gz',
                   abspatch+'/logs/'+job.name+'-sumus.log.gz')
# }}}
vertushka="-" # {{{ vertushka
def crutilka():
   global vertushka
   if "-" in vertushka: vertushka="\\"
   elif "\\" in vertushka: vertushka="|"
   elif "|" in vertushka: vertushka="/"
   else: vertushka="-" # }}}
def getshell(): # {{{получение новых версий shell и proxy
    #TODO перейти на более универсальное получение данных с сервера proxy и шелла
    cmd, stdout, stderr, return_code = shell_command(
        "namebuild=`curl -SL  ftp://192.168.10.49/download/lastbuild.wallet-jsonrpcd ` ; \
	curl -SL  ftp://192.168.10.49/download/${namebuild} | tar -C sumus --strip-components=1 -xzv ;\
        namebuild=`curl -SL  ftp://192.168.10.49/download/lastbuild.shell ` ; \
	curl -SL  ftp://192.168.10.49/download/${namebuild} | tar -xzv ; \
        mkdir sumus ; mv -f sumus-shell/*"+namesumus+"* sumus ; \
        rm -fr sumus-shell ",
    )
    if return_code: print("Error download proxy/shell") ; exit()
    #print(cmd)
    #print(stdout)
    #print(stderr)
# }}}

namesumus="mile" #karsus
versumus="0.1m"  #0.1x
##############remote

parser = createParser()
namespace = parser.parse_args(sys.argv[1:])

# {{{инициализация рабочих структур
if namespace.createconfig:
    useconfig()
    print("config template create ... updatenodes.conf")
    exit()
try:
    with open(namespace.config, 'rb') as f:
        yaml = YAML()
        obj = yaml.load(f)
except IOError:
    print("Not config data file {}".format(namespace.config))
    useconfig()
    print("config template create ... {}".format(namespace.config))
    exit()
else:
    if not useconfig(obj, create=False): print("Error config file"); exit()
    namesumus = obj['namesumus']
    versumus  = obj['versumus']
    abspatch  = obj['workdir']
    sys.path.insert(0, obj['modir'])
    import sumushell
    shell   = obj['shell']
    pemfile = obj['pemfile']
    if not namespace.external:
        namespace.external = obj['external']
    urlsumus  = obj['urlsumus']
    hosts     = obj['NODES']
    print(obj['comments'])
    print("Successfully config data load.")
    #print(hosts['node00.testnet.mile.global'])
    #print(hosts['node00.testnet.mile.global']['type'])
    #print(hosts['node00.testnet.mile.global']['wallet'])
#finally:
# }}}
if "status" in namespace.command: # {{{обработка опций запуска программы
     StartFile = "sumus"
     RootDir = "/opt/sumus"
     results=list()
     remote_task = (
          "uptime -p",
          "hostname",
    #      "md5sum"+" "+RootDir+"/"+StartFile+"|"+"cut -f 1 -d \ ",
          "rpm -qa | grep sumus ; true ",
          "systemctl status sumus &>/dev/null  && echo Run || echo Stop",
          "df --output=pcent -h / | tail -1",
         #"yum -y install ntpdate ; timedatectl set-timezone Europe/London",
         #"ntpdate -s 0.centos.pool.ntp.org 1.centos.pool.ntp.org 2.centos.pool.ntp.org ",
     )
elif "install" in namespace.command:
    abspatch = os.getcwd()
    deltareboot = random.randint(1,55)
    remote_task1 = ("systemctl status sumus && systemctl stop sumus || true ",
            "rpm -e sumus-kafkaproxy ; true",
            "rpm -e sumus ; true",)
    remote_task3 = (
            "mv -f genesis_block.txt /etc/sumus/ ",
            "chmod u=rw,og-rwx /etc/sumus/genesis_block.txt ",
            "chmod +x shell* ",
            "mv -f /root/*.wallet  /etc/sumus/wallets/ || true ",
            "chmod -R u=rwx,og-rwx /etc/sumus/wallets",
            "chown -R sumus.sumus /etc/sumus",
            "systemctl enable sumus",
        )
    urlsumus = "ftp://192.168.10.49/download/RPMBUILD/RPMS/"
    namesumusBC = "sumus-"+versumus+"-"+namesumus+".${release}.x86_64.rpm"
    if namespace.external: # {{{ для нод находящихся далеко
        cmd, stdout, stderr, return_code = shell_command(
            "curl "+urlsumus+" 2>/dev/null | grep "+namesumus+"  \
            | grep "+versumus+" | cut -f 3 -d . | sort -gr | head -n 1",
        )
        if return_code: print("Error download rpm") ; exit()
        namesumusBC = "sumus-"+versumus+"-"+namesumus+"."+str(stdout)+".x86_64.rpm"
        shell_command("curl -O "+urlsumus+namesumusBC)
        remote_task2 = ("rpm -i "+namesumusBC,)
        for host in hosts:
            remote_task2_kfkprx = tuple()
            if hosts[host]['type'] == 'kafka':
                kafkaproxy = "sumus-kafkaproxy-"+versumus+"-"+namesumus+"."+str(stdout)+".x86_64.rpm"
                shell_command("curl -O "+urlsumus+kafkaproxy)
                remote_task2_kfkprx = ("rpm -i "+namesumusBC+" "+kafkaproxy,)
                print("DETECT KAFKA")
                break
        # }}} TODO обработка ошибок
    else:
        remote_task2 = (
            "release=`curl "+urlsumus+" | grep "+namesumus+" \
            | grep "+versumus+" | cut -f 3 -d . | sort -gr | head -n 1` ; \
            curl -O "+urlsumus+namesumusBC+" ; rpm -i "+namesumusBC,
        )
    remote_task = tuple()
    remote_task_sn = tuple()
    remote_task = remote_task1 + remote_task2 + remote_task3
    getshell()
# подготовка к запуску proxy
    remote_task_sn_post = (
        "cat /etc/passwd | grep -q ^jsonrpcd || adduser -m jsonrpcd ",
        "mv -f /root/wallet-jsonrpcd ~jsonrpcd/ ; true",
        #TODO  change PEMFILE from variable
        "mv -f /root/testnet.mile.global.pem ~jsonrpcd/ ; true",
        "chown -R jsonrpcd.jsonrpcd ~jsonrpcd",
        "chmod -R o-rw ~jsonrpcd ",
        "setcap 'cap_net_bind_service=+ep' ~jsonrpcd/wallet-jsonrpcd ; true ",
        "chmod +x ~jsonrpcd/wallet-jsonrpcd ; true ",
        '''firewall-cmd --permanent --add-rich-rule='rule family="ipv4" port port=443 protocol="tcp" accept' &>/dev/null ; true''',
        "firewall-cmd --reload &>/dev/null ; true",
    )
    remote_task_sn = remote_task  + remote_task_sn_post
    print("Start of process of installation the {} on nodes.".format(namesumusBC))
elif "update" in namespace.command:
    abspatch = os.getcwd()
    remote_task1 = tuple()
    remote_task3 = (
            "chmod u=rw,og-rwx /etc/sumus/genesis_block.txt ",
            "chmod u=rwx,og-rwx /etc/sumus/wallets",
            "chown -R sumus.sumus /etc/sumus"
        )
    urlsumus = "ftp://192.168.10.49/download/RPMBUILD/RPMS/"
    namesumusBC = "sumus-"+versumus+"-"+namesumus+".${release}.x86_64.rpm"
    if namespace.external: #локальное выполнение команды
        cmd, stdout, stderr, return_code = shell_command(
            "curl "+urlsumus+" 2>/dev/null | grep "+namesumus+"  \
            | grep "+versumus+" | cut -f 3 -d . | sort -gr | head -n 1",
        )
        if return_code: print("Error download rpm") ; exit()
        namesumusBC = "sumus-"+versumus+"-"+namesumus+"."+str(stdout)+".x86_64.rpm"
        shell_command("curl -O "+urlsumus+namesumusBC)
        remote_task2 = ("rpm -U "+namesumusBC,)
        for host in hosts:
            remote_task2_kfkprx = tuple()
            if hosts[host]['type'] == 'kafka':
                kafkaproxy = "sumus-kafkaproxy-"+versumus+"-"+namesumus+"."+str(stdout)+".x86_64.rpm"
                shell_command("curl -O "+urlsumus+kafkaproxy)
                remote_task2_kfkprx = ("rpm -U "+namesumusBC+" "+kafkaproxy,)
                print("DETECT KAFKA")
                break
        #TODO обработка ошибок
    else:
        remote_task2 = (
            "release=`curl "+urlsumus+" | grep "+namesumus+" \
            | grep "+versumus+" | cut -f 3 -d . | sort -gr | head -n 1` ; \
            curl -O "+urlsumus+namesumusBC+" ; rpm -U "+namesumusBC,
        )
    remote_task = tuple()
    remote_task = remote_task2 + remote_task3
    getshell()
# обновление proxy
    remote_task_sn_post = (
        "cat /etc/passwd | grep -q ^jsonrpcd || adduser -m jsonrpcd ",
        "mv -f /root/wallet-jsonrpcd ~jsonrpcd/ ; true",
        "chown -R jsonrpcd.jsonrpcd ~jsonrpcd",
        "chmod -R o-rw ~jsonrpcd ",
        "setcap 'cap_net_bind_service=+ep' ~jsonrpcd/wallet-jsonrpcd ; true"
    )
    remote_task_sn = remote_task  + remote_task_sn_post
    print("Start of process updating of the {} on nodes.".format(namesumusBC))
elif "erase" in namespace.command:
    remote_task = (
        "systemctl status sumus && systemctl stop sumus || true ",
        "lists=`ps ax | grep su[m]us | sed 's/^[ ]*//g' | cut -f 1 -d \" \"`; kill ${lists}; true",
        "rpm -e sumus-kafkaproxy ; true",
        "rpm -e sumus ; true",
        '''firewall-cmd --permanent --remove-rich-rule='rule family="ipv4" port port=443 protocol="tcp" accept' &>/dev/null ; true''',
        "rm -fr /etc/sumus /opt/sumus /var/log/sumus"
    )
elif "start" == namespace.command:
    remote_task = (
        "systemctl status sumus && true || systemctl start sumus ",
    )
    remote_task_sn = remote_task + ( # старт proxy
        "nohup su jsonrpcd -lc './wallet-jsonrpcd -m 127.0.0.1 -p 443 -s testnet.mile.global.pem'  &> /dev/null & echo $! > /var/run/lock/jsonrpcd.pid",
    )
elif "stop" in namespace.command:
    remote_task = tuple()
    remote_task = (
        "systemctl status sumus && systemctl stop sumus || true ",
    )
    remote_task_sn = remote_task + ( # stop proxy
        "KILLPID=$(</var/run/lock/jsonrpcd.pid); kill $KILLPID ; >/var/run/lock/jsonrpcd.pid",
    )
elif "restart" == namespace.command:
    remote_task = (
        "systemctl status sumus && systemctl stop sumus || true ",
        "systemctl start sumus"
    )
elif "copylog" in namespace.command:
    abspatch = os.getcwd()
    remote_task = (
        "systemctl status sumus && echo > /tmp/.strn || rm -f /tmp/.strn &>/dev/null",
        "if [ -e /tmp/.strn ] ; then systemctl stop sumus ; fi",
        "mv /var/log/sumus/sumus.log   /var/log/sumus/export.log",
        "rm /var/log/sumus/export.log.gz",
        "gzip /var/log/sumus/export.log",
        "if [ -e /tmp/.strn ] ; then systemctl start sumus ; fi",
        "rm -f /tmp/.strn &>/dev/null"
    )
else: exit() # }}}
jobs=list()
myhookpre = Hook(target=sshtransfer)
myhookpost = Hook(target=sshobtaining)
for host in hosts:
    if namespace.host:
        host = namespace.host
        if not hosts.get(host):
            print("Not found host: {} from config file {}".format(host, namespace.config))
            exit()
 # {{{ install section
    if "install" in namespace.command or "update" in namespace.command :
        if hosts[host]['type'] == 'work':
            remote_work = remote_task1 + remote_task2 \
               + remote_task3 + ('''sed -i~ '/^OPT1/ s/".*"/"'''\
               +obj['addoptionstartnodework'] \
               +'''"/' /etc/sumus/sumus.conf ; true''',)
            job = ServerJob(host, remote_work, username='root', password=None,
                  timeout=(30,3600),prehook=myhookpre
              )
        elif hosts[host]['type'] == 'passive':
            remote_passive = remote_task1 + remote_task2 \
                + remote_task3 + remote_task_sn_post \
                + ('''sed -i~ '/^OPT1/ s/".*"/"'''\
                +obj['addoptionstartnodesn'] \
                +'''"/' /etc/sumus/sumus.conf ; true''',)
            job = ServerJob(host, remote_passive, username='root', password=None,
                  timeout=(30,3600),prehook=myhookpre
              )
        elif hosts[host]['type'] == 'kafka':
            remote_kafka = remote_task1 + remote_task2_kfkprx \
                + remote_task3 + remote_task_sn_post \
                + ('''sed -i~ '/^OPT2/ s/".*"/"'''\
                +''' --kafka"/' /etc/sumus/sumus.conf ; true''',)
            job = ServerJob(host, remote_kafka, username='root', password=None,
                  timeout=(30,3600),prehook=myhookpre
              )
        else:
            job = ServerJob(host, remote_task_sn, username='root', password=None,
                  timeout=(30,3600),prehook=myhookpre
              ) # }}}
# {{{update section
    elif "start" == namespace.command or \
         "stop" == namespace.command:
        if hosts[host]['type'] == 'work':
            job = ServerJob(host, remote_task, username='root', password=None,
                  timeout=(30,3600)
              )
        else:
            job = ServerJob(host, remote_task_sn, username='root', password=None,
                  timeout=(30,3600)
              ) # }}}
    elif "copylog" in namespace.command:
        job = ServerJob(host, remote_task, username='root', password=None,
                  timeout=(30,3600), posthook=myhookpost
              )
    else:
        job = ServerJob(host, remote_task, username='root', password=None, timeout=(30,3600))
    jobs.append(job)
    if namespace.host: break

print("")
count = 0
#снижение нагрузки на локальный вирт.сервер
tcount_ = 5 if not namespace.external and namespace.command == "copylog" else 15
#print(tcount_)
finished = sshread(jobs, tcount=tcount_, progress_bar=True)
if "status" in namespace.command: results=list()
for job in finished:
    if not job.status:
        count += 1
        if "status" in namespace.command:
            results.append(
                namesumus+" status:"+job.results[3].stdout+
                " on a host:"+job.results[1].stdout+
                " Disk usages:"+job.results[4].stdout+" "+
                "rpm:"+job.results[2].stdout+
                " "+job.results[0].stdout
            )
        elif  "install" in namespace.command:
            pass
            #print(job.results)

    else:
        print("Error host:", job.name)
        print("Result:", job.results)

if not namespace.host: print("All hosts:",len(hosts),"Error host:",len(hosts)-count)

if "status" in namespace.command:
    for res in sorted(set(results)): print(res)
#print("Hello world","\r",end="")
#print("Bye  ")
#time.sleep(2)
sys.exit()
